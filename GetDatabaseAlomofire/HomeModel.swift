//
//  HomeModel.swift
//  GetDatabaseAlomofire
//
//  Created by RISNA on 11/28/17.
//  Copyright © 2017 RISNA. All rights reserved.
//

import Foundation
protocol HomeModelProtocol: class {
    func itemsDownloaded(items: NSArray)
}

class HomeModel: NSObject, URLSessionDataDelegate {
    
    //properties
    weak var delegate: HomeModelProtocol!
    var data = Data()
    let urlPath: String = "http://192.168.10.10/api/authors"
    
    func downloadItems() {
        
        let url = URL(string: "http://192.168.10.10/graphql")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let postString = "query={authors{name,uri,description}}"
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                //print("statusCode should be 200, but is \(httpStatus.statusCode)")
                //print("response = \(response)")
            }
            
            let responseString = String(data: data, encoding: .utf8)
            //print("responseString = \(responseString)")
            self.postJSON(data)
        }
        task.resume()
    }
    
    func postJSON(_ data:Data) {
        let authors = NSMutableArray()
        do{
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            let result = json!!["data"] as! [String: Any]
            let authorTemp =  result["authors"] as! NSArray
            
            authorTemp.forEach { item in
                
                let author = AuthorModel()
                
                if let name = (item as AnyObject)["name"] as? String{
                    author.name = name
                }
                if let uri = (item as AnyObject)["uri"] as? String{
                    author.uri = uri
                }
                if let descriptionAuthor = (item as AnyObject)["description"] as? String{
                    author.descriptionAuthor = descriptionAuthor
                }
                
                authors.add(author)
            }
        } catch let error as NSError {
            print(error)
            
        }
        
        DispatchQueue.main.async(execute: { () -> Void in
            
            self.delegate.itemsDownloaded(items: authors)
            
        })
    }
}

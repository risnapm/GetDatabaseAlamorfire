//
//  AuthorModel.swift
//  GetDatabaseAlomofire
//
//  Created by RISNA on 11/28/17.
//  Copyright © 2017 RISNA. All rights reserved.
//

import Foundation
class AuthorModel : NSObject {
    
    // property
    var name : String?
    var uri : String?
    var descriptionAuthor : String?
    
    //empty constructor
    override init() {
        
    }
    
    //construct with @name, @uri, @descriptionAuthor parameters
    init(name: String, uri: String, descriptionAuthor: String) {
        
        self.name = name
        self.uri = uri
        self.descriptionAuthor = descriptionAuthor
        
    }
    
    //prints object's current state
    override var description: String {
        return "Name: \(name), uri: \(uri), descrition: \(descriptionAuthor)"
        
    }
    
}
